Async Mailer
============

An HTTP server accepts an email address and an HTML page
and asynchronously sends it to an SMTP server for delivery.

Motivation
----------

To send an email from PHP there are three ways to do that:

1. Use built-in `mail` function.
2. Use sockets to connect to an SMTP server.
3. Store the email locally and send later with
a cron script using the previous ways.

### Disadvantages of `mail` function

The built-in `mail` function requires some kind of a message transfer agent
(MTA) to be installed and configured on the host. The MTA configuration
requires steps which in an organisation you may not be allowed to do.
These steps include:

* Obtaining a private key for digitally signing all the outgoing messages.
* Modifying DNS records - adding/editing SPF and DKIM.
* Port forwarding if the sending machine is behind NAT.

Mails sent from an incorrectly configured MTA is
likely to end up in a spam folder.

### Disadvantages of using sockets

Using sockets is synchronous and SMTP negotiation is slow.
A PHP page will not finish loading until an SMTP server fully
receives the message body. This may take a few seconds in general.
But if you're sending plenty of different emails at the same time
you're likely to face a delay of tens of seconds.

### Disadvantages of sending with cron

Cron runs at most once a minute. In the worst case an email sent at 00:00:01
will actually be sent at 00:01:00. The 59 second delay is bad, especially
when a user waits for a link to reset his/her account password.

### The solution

Async Mailer addresses this issues. It is supposed to run on localhost.
It will receive the mail via HTTP using `curl`. Thus queuing even a
hundred of emails is a matter of a second. Sending is started immediately.
No need for a local MTA configuration. No delay to users.
No delay in sending email. Happy customers.

Scripts
-------

* `./restart.sh` - start/restart the server.
* `./stop.sh` - stop the server.
* `./clean.sh` - clean the server after an unexpected shutdown.

Configuration
-------------

`config.js` contains the configuration.

Usage in PHP
------------
```
$ch = curl_init('http://localhost:9080/');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
    'to' => 'user@example.com',
    'subject' => 'Hello',
    'html' => 'Hello <b>world!</b>',
]));
curl_exec($ch);
```

Technology
----------

* Node.js
* `nodemailer` NPM package
