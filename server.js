const http = require('http'),
    querystring = require('querystring')

const nodemailer = require('nodemailer')

const CleanAttachments = require('./lib/CleanAttachments.js'),
    PrepareAttachments = require('./lib/PrepareAttachments.js'),
    ReadText = require('./lib/ReadText.js'),
    RequestRecipients = require('./lib/RequestRecipients.js')

const config = require('./config.js')

const configListen = config.listen,
    configSmtp = config.smtp

http.createServer((req, res) => {
    ReadText(req, requestText => {

        const params = querystring.parse(requestText)

        res.setHeader('Content-Type', 'application/json')
        res.end('true')

        const transport = nodemailer.createTransport({
            host: configSmtp.host,
            port: configSmtp.port,
            auth: {
                user: configSmtp.username,
                pass: configSmtp.password,
            },
        })

        const options = {
            from: configSmtp.from,
            to: String(params.to),
            cc: RequestRecipients(params, 'cc'),
            bcc: RequestRecipients(params, 'bcc'),
            subject: String(params.subject),
            html: String(params.html),
        }

        const reply_to = params.reply_to
        if (reply_to !== undefined) options.replyTo = reply_to

        const list_unsubscribe = params.list_unsubscribe
        if (list_unsubscribe !== undefined) {
            options.list = { unsubscribe: list_unsubscribe }
        }

        PrepareAttachments(params, attachments => {
            options.attachments = attachments
            transport.sendMail(options, (err, info) => {

                CleanAttachments(attachments)

                if (err) {
                    console.log((new Date).toISOString(), 'ERROR', err)
                    return
                }

                console.log((new Date).toISOString(), 'SENT', info)

            })
        })

    })
}).listen(configListen.port, configListen.host)
