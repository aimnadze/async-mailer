const nodemailer = require('nodemailer')

module.exports = app => (log, to, item, done) => {

    function send (trial) {
        ;(log => {

            log.info('SENDING')

            const transport = nodemailer.createTransport({
                host: smtp.host,
                port: smtp.port,
                auth: {
                    user: smtp.username,
                    pass: smtp.password,
                },
            })

            const params = item.params

            const options = {
                from: smtp.from,
                to: to,
                cc: app.RequestRecipients(params, 'cc'),
                bcc: app.RequestRecipients(params, 'bcc'),
                subject: String(params.subject),
                html: String(params.html),
            }

            const reply_to = params.reply_to
            if (reply_to !== undefined) options.replyTo = reply_to

            const list_unsubscribe = params.list_unsubscribe
            if (list_unsubscribe !== undefined) {
                options.list = { unsubscribe: list_unsubscribe }
            }

            app.PrepareAttachments(params, attachments => {
                options.attachments = attachments
                transport.sendMail(options, (err, info) => {

                    app.CleanAttachments(attachments)

                    if (err) {

                        log.info('ERROR', err.code)

                        if (trial === 16) {
                            log.info('ABANDONED')
                            done()
                            return
                        }

                        trial++
                        const delay = trial * trial
                        log.info('RETRYING', 'DELAY=' + delay)
                        setTimeout(() => {
                            send(trial)
                        }, delay * 1000)
                        return

                    }

                    log.info('SENT', JSON.stringify(info))
                    done()

                })
            })

        })(log.sublog('ID=' + item.id + ' TRIAL=' + trial))
    }

    const smtp = app.config.smtp
    send(0)

}
