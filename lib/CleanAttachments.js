const fs = require('fs')

module.exports = attachments => {
    attachments.forEach(attachment => {
        fs.unlink(attachment.path, () => {})
    })
}
