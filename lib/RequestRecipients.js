module.exports = (params, field) => {
    const array = []
    let i = 0
    while (true) {
        let item = params[field + '[' + i + ']']
        if (item === undefined) break
        array.push(item)
        i++
    }
    return array
}
