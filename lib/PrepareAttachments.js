const crypto = require('crypto'),
    fs = require('fs'),
    os = require('os')

const tmpdir = os.tmpdir()

module.exports = (params, callback) => {

    function next () {

        const index = attachments.length

        const name = params['attachments[' + index + '][name]']
        if (name === undefined) {
            callback(attachments)
            return
        }

        const type = (() => {
            const type = params['attachments[' + index + '][type]']
            return type === undefined ? 'application/octet-stream' : type
        })()

        const disposition = (() => {
            const disposition = params['attachments[' + index + '][disposition]']
            return disposition === undefined ? 'attachment' : disposition
        })()

        const content = Buffer.from(params['attachments[' + index + '][content]'], 'base64')

        const path = tmpdir + '/' + crypto.randomBytes(10).toString('hex')

        fs.writeFile(path, content, () => {
            attachments.push({
                filename: name,
                cid: name,
                path: path,
                contentType: type,
                contentDisposition: disposition,
            })
            next()
        })

    }

    const attachments = []
    next()

}
