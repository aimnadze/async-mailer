const http = require('http')
const querystring = require('querystring')

module.exports = app => () => {

    const listen = app.config.listen

    const queues = Object.create(null)

    http.createServer((req, res) => {
        app.ReadText(req, requestText => {

            const params = querystring.parse(requestText)

            res.setHeader('Content-Type', 'application/json')
            res.end('true')

            const to = String(params.to)
            if (queues[to] === undefined) {
                const log = app.log.sublog(to)
                log.info('QUEUE CREATED')
                queues[to] = app.Queue(log, to, () => {
                    log.info('QUEUE DELETED')
                    delete queues[to]
                })
            }

            queues[to](params)

        })
    }).listen(listen.port, listen.host)

    app.Watch(['lib'])

    app.log.info('Started')
    app.log.info('Listening http://' + listen.host + ':' + listen.port + '/')

}
