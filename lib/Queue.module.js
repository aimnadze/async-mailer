module.exports = app => (log, to, done) => {

    function next () {
        if (busy) return
        busy = true
        const item = queue.shift()
        log.info('ID=' + item.id, 'SENDING')
        app.Send(log, to, item, () => {

            if (queue.length === 0) {
                done()
                return
            }

            busy = false
            next()

        })
    }

    let busy = false
    let id = 0
    const queue = []

    return params => {
        id++
        log.info('ID=' + id, 'ENQUEUED')
        queue.push({ id, params })
        next()
    }

}
