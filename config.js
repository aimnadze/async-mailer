exports.listen = {
    host: '127.0.0.1',
    port: 9080,
}

exports.smtp = {
    host: 'localhost',
    port: 587,
    from: 'user@localhost',
    username: 'user',
    password: '',
}
