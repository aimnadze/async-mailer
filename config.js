module.exports = {
    debug_mode: true,
    listen: {
        port: 9080,
        host: '127.0.0.1',
    },
    smtp: {
        host: 'localhost',
        port: 587,
        from: 'user@localhost',
        username: 'user',
        password: '',
    },
}
