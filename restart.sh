#!/bin/bash
cd `dirname $BASH_SOURCE`

echo 'INFO: Testing config ...'
node config.js || exit 1

./stop.sh
./logrotate.sh

echo 'INFO: Starting server ...'
node server.js >> log/server.out 2>> log/server.err &
echo $! > server.pid
sleep 0.2
cat log/server.out log/server.err
