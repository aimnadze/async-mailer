#!/bin/bash
cd `dirname $BASH_SOURCE`

if [ ! -f index.pid ]
then
    exit
fi

if ps `cat index.pid` > /dev/null
then
    echo 'ERROR Still running'
    exit 1
fi

echo 'INFO Cleaning'
rm index.pid
